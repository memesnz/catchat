defmodule Ed25519.Dalek do
  use Rustler, otp_app: :catchat, crate: "erl_ed25519_dalek"

  # When your NIF is loaded, it will override this function.
  def signature(_data, _sk, _pk), do: :erlang.nif_error(:nif_not_loaded)
  def generate_key_pair(_seed), do: :erlang.nif_error(:nif_not_loaded)
end
