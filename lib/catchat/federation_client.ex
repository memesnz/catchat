defmodule CatChat.FederationClient do
  import CatChat.FederationClient.Macros

  # TODO sig not required for these four, don't bother computing it
  defget server_version, "/_matrix/federation/v1/version"
  defget server_keys, "/_matrix/key/v2/server/:key_id"
  defget server_keys_indirect, "/_matrix/key/v2/query/:server_name/:key_id"
  defpost server_keys_indirect_batch, "/_matrix/key/v2/query"

  defput send_txn, "/_matrix/federation/v1/send/:txn_id"
  defget event_auth, "/_matrix/federation/v1/event_auth/:room_id/:event_id"
  defpost query_auth, "/_matrix/federation/v1/query_auth/:room_id/:event_id"
  defget backfill, "/_matrix/federation/v1/backfill/:room_id"
  defpost get_missing_events, "/_matrix/federation/v1/get_missing_events/:room_id"
  defget state, "/_matrix/federation/v1/state/:room_id"
  defget state_ids, "/_matrix/federation/v1/state_ids/:room_id"
  defget event, "/_matrix/federation/v1/event/:event_id"
  defget make_join, "/_matrix/federation/v1/make_join/:room_id/:user_id"
  defput send_join, "/_matrix/federation/v1/send_join/:room_id/:event_id"
  # TODO needed?
  # defput invite_v1, "/_matrix/federation/v1/invite/:room_id/:event_id"
  defput invite, "/_matrix/federation/v2/invite/:room_id/:event_id"
  defget make_leave, "/_matrix/federation/v1/make_leave/:room_id/:user_id"
  defput send_leave, "/_matrix/federation/v1/send_leave/:room_id/:event_id"
  defput exchange_third_party_invite, "/_matrix/federation/v1/exchange_third_party_invite/:room_id"
  # TODO only used by identity servers?
  # defput , "/_matrix/federation/v1/3pid/onbind"
  defget public_rooms, "/_matrix/federation/v1/public_rooms"
  defget query_directory, "/_matrix/federation/v1/query/directory"
  defget query_profile, "/_matrix/federation/v1/query/profile"
  # TODO do we need this?
  # defget openid_userinfo, "/_matrix/federation/v1/openid/userinfo"
  defget devices, "/_matrix/federation/v1/user/devices/:user_id"
  defpost claim_keys, "/_matrix/federation/v1/user/keys/claim"
  defpost query_keys, "/_matrix/federation/v1/user/keys/query"
end
