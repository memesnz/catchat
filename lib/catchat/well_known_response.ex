defmodule CatChat.NameResolution.WellKnown.Response do
  use Ecto.Schema
  @primary_key false

  schema "well_known_cache" do
    field :server_name, :string, primary_key: true

    # If not an error, will be populated with result
    field :m_server, :string

    # If an error and has expired, this time should be doubled and used for the next backoff
    field :last_backoff_secs, :integer

    field :valid_until, :utc_datetime
  end
end
