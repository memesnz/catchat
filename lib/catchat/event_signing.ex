defmodule CatChat.EventSigning do
  def hash_and_sign(map) do
    map
    |> hash_and_sign(
      Application.get_env(:catchat, :active_keypair),
      Application.get_env(:catchat, :server_name)
    )
  end

  def hash_and_sign(event, keypair, server_name) do
    event = add_content_hash(event)

    Map.put(
      event,
      "signatures",
      (event |> redact_event |> CatChat.JsonSigning.sign(keypair, server_name))["signatures"]
    )
  end

  defp add_content_hash(event) do
    hash =
      event
      |> Map.drop(~w(unsigned signatures hashes))
      |> CatChat.CanonicalJson.encode!()
      |> (&:crypto.hash(:sha256, &1)).()
      |> CatChat.UnpaddedBase64.encode()

    Map.put(event, "hashes", %{"sha256" => hash})
  end

  @nonredacted_keys ~w(event_id type room_id sender state_key prev_content content hashes signatures depth prev_events prev_state auth_events origin origin_server_ts membership)
  @nonredacted_content %{
    "m.room.member" => ~w(membership),
    "m.room.create" => ~w(creator),
    "m.room.join_rules" => ~w(join_rule),
    "m.room.power_levels" =>
      ~w(ban events events_default kick redact state_default users users_default),
    "m.room.aliases" => ~w(aliases),
    "m.room.history_visibility" => ~w(history_visibility)
  }

  defp redact_event(event) do
    type = event[:type]

    event
    |> Map.take(@nonredacted_keys)
    |> Map.update("content", %{}, &Map.take(&1, Map.get(@nonredacted_content, type, %{})))
  end
end
