defmodule CatChat.UnpaddedBase64 do
  def encode(x) do
    Base.encode64(x, padding: false)
  end

  def decode!(x) do
    Base.decode64!(x, padding: false)
  end
end
