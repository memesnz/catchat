defmodule CatChat.Key do
  @enforce_keys [:pk, :algorithm, :version]
  defstruct [:pk, :algorithm, :version]
end
