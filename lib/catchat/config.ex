defmodule CatChat.Config do
  def load() do
    {sk, pk} =
      Ed25519.Dalek.generate_key_pair(
        CatChat.UnpaddedBase64.decode!("YJDBA9Xnr2sVqXD9Vj7XVUnmFZcZrlw8Md7kMW+3XA1")
      )

    Application.put_env(:catchat, :server_name, "terrible-yak-2.localtunnel.me:443")

    Application.put_env(:catchat, :active_keypair, %CatChat.KeyPair{
      sk: sk,
      pk: pk,
      algorithm: "ed25519",
      version: "1"
    })

    Application.put_env(:catchat, :retired_keys, [])
    Application.put_env(:catchat, :key_ttl_minutes, 1440)
  end

  def get(key) do
    Application.get_env(:catchat, key)
  end
end
