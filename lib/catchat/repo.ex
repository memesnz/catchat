defmodule CatChat.Repo do
  use Ecto.Repo,
    otp_app: :catchat,
    adapter: Ecto.Adapters.Postgres
end
