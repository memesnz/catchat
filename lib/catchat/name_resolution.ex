defmodule CatChat.NameResolution do
  alias CatChat.NameResolution.WellKnown

  def resolve!(server_name) do
    case resolve(server_name) do
      {:ok, result} -> result
      {:error, error} -> raise error
    end
  end

  def resolve(server_name) do
    case parse_host_port(server_name) do
      {:ip, ip, port} ->
        {:ok, {ip, port, server_name, nil}}

      {:ip, ip} ->
        {:ok, {ip, 8448, ip, nil}}

      {:domain, domain, port} ->
        {:ok, {domain, port, server_name, domain}}

      {:domain, domain} ->
        case WellKnown.lookup(domain) do
          {:ok, delegated_hostname} ->
            case parse_host_port(delegated_hostname) do
              {:ip, ip, port} ->
                {:ok, {ip, port, delegated_hostname, nil}}

              {:ip, ip} ->
                {:ok, {ip, 8448, ip, nil}}

              {:domain, domain, port} ->
                {:ok, {domain, port, domain, domain}}

              {:domain, domain} ->
                case srv_lookup(domain) do
                  {:ok, {host, port}} ->
                    {:ok, {host, port, delegated_hostname, delegated_hostname}}

                  :error ->
                    {:ok, {delegated_hostname, 8448, delegated_hostname, delegated_hostname}}
                end

              {:error, error} ->
                {:error, error}
            end

          {:error, _} ->
            case srv_lookup(domain) do
              {:ok, {host, port}} ->
                {:ok, {host, port, domain, domain}}

              :error ->
                {:ok, {domain, 8448, domain, domain}}
            end
        end

      {:error, error} ->
        {:error, error}
    end
  end

  def to_list(ip) do
    # example input: "10.0.0.1"
    # ["10", "0", "0", "1"]
    segments = String.split(ip, ".")
    # [10,0,0,1]
    for segment <- segments, do: String.to_integer(segment)
  end

  def is_ipv4?(ip) do
    case Regex.match?(~r/^(\d{1,3}\.){3}\d{1,3}$/, ip) do
      false -> false
      true -> ip |> to_list |> Enum.any?(fn x -> x > 255 end) |> Kernel.not()
    end
  end

  def is_ipv6?(ip) do
    case(Regex.run(~r/^\[(.*)\]$/, ip)) do
      [_, ip] ->
        case :inet.parse_ipv6strict_address(to_charlist(ip)) do
          {:ok, _} -> true
          _ -> false
        end

      _ ->
        false
    end
  end

  def is_ip?(ip) do
    is_ipv4?(ip) or is_ipv6?(ip)
  end

  def parse_host_port(hostname) do
    case Regex.run(~r/^(.*):(\d{1,5})$/, hostname) do
      [_, host, port] ->
        {port, _} = Integer.parse(port)

        if port > 65_535 do
          {:error, :highport}
        else
          if is_ip?(host) do
            {:ip, host, port}
          else
            {:domain, host, port}
          end
        end

      _ ->
        if is_ip?(hostname) do
          {:ip, hostname}
        else
          {:domain, hostname}
        end
    end
  end

  def srv_lookup(hostname) do
    case :inet_res.lookup('_matrix._tcp.#{hostname}', :in, :srv) do
      # We currently ignore all but the first result
      [{_, _, port, host} | _] -> {:ok, {host, port}}
      _ -> :error
    end
  end
end
