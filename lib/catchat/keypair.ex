defmodule CatChat.KeyPair do
  @enforce_keys [:sk, :pk, :algorithm, :version]
  defstruct [:sk, :pk, :algorithm, :version]

  def id(key) do
    "#{key.algorithm}:#{key.version}"
  end

  def parse_id(id) do
    [alg, ver] = String.split(id, ":")
    {alg, ver}
  end
end
