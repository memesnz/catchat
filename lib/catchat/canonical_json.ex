defmodule CatChat.CanonicalJson do
  def encode!(x) do
    Jason.encode!(x)
  end

  def encode_to_iodata!(x) do
    Jason.encode_to_iodata!(x)
  end
end
