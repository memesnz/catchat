defmodule CatChat.NameResolution.WellKnown do
  alias CatChat.NameResolution.WellKnown.Response

  def max_age(headers) do
    cc =
      headers
      |> Enum.find_value(fn {k, v} ->
        if String.downcase(k) == "cache-control", do: v, else: false
      end)

    if cc != nil do
      cc
      |> String.split(~r{,\s*}, trim: true)
      |> Enum.find_value(fn
        "max-age=" <> age ->
          case Integer.parse(age) do
            {age_i, _} -> age_i
            _ -> false
          end

        _ ->
          false
      end)
    end
  end

  def lookup_direct(hostname) do
    case HTTPoison.get("#{hostname}/.well-known/matrix/server", [], follow_redirect: true) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body, headers: headers}} ->
        case Jason.decode(body) do
          {:ok, %{"m.server" => server}} -> {:ok, {server, headers |> max_age}}
          {:error, _} -> {:error, :jsonerr}
        end

      _ ->
        {:error, :reqerr}
    end
  end

  defp store_response(server_name, {:ok, {m_server, max_age}}, _) do
    max_age =
      case max_age do
        nil -> 24 * 60 * 60
        a -> min(a, 48 * 60 * 60)
      end

    %Response{
      server_name: server_name,
      m_server: m_server,
      valid_until:
        DateTime.from_unix!((DateTime.utc_now() |> DateTime.to_unix(:second)) + max_age, :second)
    }
    |> CatChat.Repo.insert!(conflict_target: [:server_name], on_conflict: :replace_all)
  end

  defp store_response(server_name, {:error, _}, last_backoff) do
    backoff =
      case last_backoff do
        nil -> 30
        a -> min(a * 2, 60 * 60)
      end

    %Response{
      server_name: server_name,
      last_backoff_secs: backoff,
      valid_until:
        DateTime.from_unix!((DateTime.utc_now() |> DateTime.to_unix(:second)) + backoff, :second)
    }
    |> CatChat.Repo.insert!(conflict_target: [:server_name], on_conflict: :replace_all)
  end

  def lookup(server_name) do
    # TODO
    # require Ecto.Query
    cached = Response |> CatChat.Repo.get_by(server_name: server_name)

    if cached != nil and DateTime.compare(cached.valid_until, DateTime.utc_now()) == :gt do
      case cached do
        %Response{m_server: nil} -> {:error, :cached_error}
        %Response{m_server: s} -> {:ok, s}
      end
    else
      resp = lookup_direct(server_name)

      Task.start(fn ->
        store_response(server_name, resp, if(cached, do: cached.last_backoff_secs))
      end)

      case resp do
        {:ok, {m_server, _}} -> {:ok, m_server}
        {:error, e} -> {:error, e}
      end
    end
  end
end
