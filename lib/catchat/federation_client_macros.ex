defmodule CatChat.FederationClient.Macros do
  defp parse_path(path) do
    path
    |> String.split("/", trim: true)
    |> Enum.map(
      &case(&1) do
        ":" <> name -> String.to_atom(name)
        path_part -> path_part
      end
    )
  end

  defp path_format_string(path) do
    {:<<>>, [],
     path
     |> Enum.map(fn
       a when is_atom(a) ->
         # TODO
         quote do:
                 URI.encode(unquote(Macro.var(a, __MODULE__)), &URI.char_unreserved?/1) :: binary

       part ->
         part
     end)
     |> Enum.flat_map(&["/", &1])}
  end

  defmacro defget(name, path) do
    defendpoint_ast(name, path, :get)
  end

  defmacro defput(name, path) do
    defendpoint_ast(name, path, :put)
  end


  defmacro defpost(name, path) do
    defendpoint_ast(name, path, :post)
  end


  defp defendpoint_ast({name, _, _}, path, method) do
    path = parse_path(path)

    url = path |> path_format_string
    quote do
      def unquote(name)(
            server,
            unquote_splicing(
              path
              |> Enum.filter(&is_atom(&1))
              |> Enum.map(&Macro.var(&1, __MODULE__))
            ),
            params \\ []
          ) do

        apply(CatChat.FederationClient.HTTP, unquote(method), [server, unquote(url), params])
      end
    end
  end
end
