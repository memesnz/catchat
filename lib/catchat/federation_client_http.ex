defmodule CatChat.FederationClient.HTTP do
  @method_names %{get: "GET", post: "POST", put: "PUT"}

  def authorization(keypair, server_name, method, server, path, body \\ nil) do
    map = %{
      method: Map.fetch!(@method_names, method),
      uri: path,
      origin: server_name,
      destination: server
    }

    map =
      case body do
        nil -> map
        cj -> Map.put(map, :content, cj)
      end

    [
      "X-Matrix origin=",
      server_name,
      ",key=\"",
      keypair.algorithm,
      ":",
      keypair.version,
      "\",sig=\"",
      CatChat.JsonSigning.make_signature(map, keypair),
      "\""
    ]
  end

  def get(server, path, params \\ []) do
    path =
      case params do
        [] -> path
        params -> "#{path}?#{URI.encode_query(params)}"
      end

    do_request(:get, server, path)
  end

  def put(server, path, params \\ []) do
    content_json = CatChat.CanonicalJson.encode!(Map.new(params))
    do_request(:put, server, path, content_json)
  end

  def post(server, path, params \\ []) do
    content_json = CatChat.CanonicalJson.encode!(Map.new(params))
    do_request(:post, server, path, content_json)
  end

  defp do_request(method, server, path, content_json \\ "") do
    auth =
      authorization(
        Application.get_env(:catchat, :active_keypair),
        Application.get_env(:catchat, :server_name),
        method,
        server,
        path
      )

    case CatChat.NameResolution.resolve(server) do
      {:ok, {conn_host, port, header_host, sni_host}} ->
        HTTPoison.request(
          method,
          "https://#{conn_host}:#{port}#{path}",
          content_json,
          [Authorization: auth, Host: header_host],
          recv_timeout: 50_000,
          ssl: [server_name_indication: sni_host |> to_charlist]
        )
      e -> e
    end
  end
end
