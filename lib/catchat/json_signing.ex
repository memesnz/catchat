defmodule CatChat.JsonSigning do
  def make_signature(map, keypair) do
    map
    |> Map.delete("unsigned")
    |> Map.delete("signatures")
    |> CatChat.CanonicalJson.encode!()
    |> Ed25519.Dalek.signature(keypair.sk, keypair.pk)
    |> CatChat.UnpaddedBase64.encode()
  end

  def sign(map) do
    map
    |> sign(
      Application.get_env(:catchat, :active_keypair),
      Application.get_env(:catchat, :server_name)
    )
  end

  def sign(map, keypair, server_name) do
    sig = make_signature(map, keypair)

    map
    |> Map.put_new("signatures", %{})
    |> Map.update!("signatures", &Map.put_new(&1, server_name, %{}))
    |> put_in(["signatures", server_name, CatChat.KeyPair.id(keypair)], sig)
  end
end
