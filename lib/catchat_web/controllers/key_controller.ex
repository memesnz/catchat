defmodule CatChatWeb.KeyController do
  use CatChatWeb, :controller
  alias CatChat.Config

  def show(conn, _params) do
    render(conn, "server.json", %{
      server_name: Config.get(:server_name),
      keys: [Config.get(:active_keypair)],
      old_keys: Config.get(:retired_keys),
      valid_until: (DateTime.utc_now() |> DateTime.to_unix(:millisecond)) + Config.get(:key_ttl_minutes) * 60 * 1000
    })
  end
end
