defmodule CatChatWeb.WoxController do
  use CatChatWeb, :controller

  def awoo(conn, _params) do
    text(conn, "awoo!!!")
  end
end
