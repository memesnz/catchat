defmodule CatChatWeb.IndexController do
  use CatChatWeb, :controller

  def index(conn, _params) do
    text(conn, "im cat")
  end
end
