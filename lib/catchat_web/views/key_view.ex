defmodule CatChatWeb.KeyView do
  use CatChatWeb, :view

  def render("server.json", %{
        server_name: server_name,
        keys: keys,
        valid_until: valid_until
      }) do
    %{
      server_name: server_name,
      verify_keys:
        keys
        |> Stream.map(fn key ->
          {key |> CatChat.KeyPair.id(),
           %{
             key: key.pk |> CatChat.UnpaddedBase64.encode()
           }}
        end)
        |> Map.new(),
      old_verify_keys: %{},
      valid_until_ts: valid_until
    }
    |> CatChat.JsonSigning.sign()
  end
end
