defmodule CatChatWeb.Router do
  use CatChatWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CatChatWeb do
    pipe_through :api

    get "/", IndexController, :index

    scope "/_matrix" do
      scope "/key" do
        # Path parameter is deprecated and assumed optional
        get "/v2/server", KeyController, :show
        get "/v2/server/:id", KeyController, :show
      end

      scope "/federation" do
        # TODO federate
      end

      get "/awoo", WoxController, :awoo
    end
  end
end
