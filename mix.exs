defmodule CatChat.MixProject do
  use Mix.Project

  def project do
    dialyzer_confs =
      if Mix.env() == :ci_dev do
        [plt_file: {:no_warn, "priv/plts/dialyzer.plt"}]
      else
        []
      end

    [
      app: :catchat,
      version: "0.1.0",
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext, :rustler] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      rustler_crates: [erl_ed25519_dalek: []],
      dialyzer: [plt_add_deps: :transitive] ++ dialyzer_confs
    ] ++
      if Mix.env() == :ci do
        [test_coverage: [tool: ExCoveralls]]
      else
        []
      end
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {CatChat.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(:ci), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.4.6"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_ecto, "~> 4.0"},
      {:ecto_sql, "~> 3.0"},
      {:postgrex, ">= 0.0.0"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},
      {:ed25519, "~> 1.3"},
      {:rustler, "~> 0.20.0"},
      {:httpoison, "~> 1.5.1"},
      {:credo, "~> 1.0.0", only: [:dev, :test], runtime: false},
      {:benchee, "~> 1.0", only: :dev},
      {:dialyxir, "~> 0.5", only: [:dev, :ci_dev], runtime: false},
      {:ex_doc, "~> 0.19", only: :dev, runtime: false},
      {:junit_formatter, "~> 3.0", only: :ci},
      {:excoveralls, "~> 0.10", only: :ci}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
