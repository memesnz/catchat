{sk, pk} =
  Ed25519.generate_key_pair(
    CatChat.UnpaddedBase64.decode!("YJDBA9Xnr2sVqXD9Vj7XVUnmFZcZrlw8Md7kMW+3XA1")
  )

data =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

cases = %{
  "Ed25519" => fn -> Ed25519.signature(data, sk, pk) end,
  "Dalek" => fn -> Ed25519.Dalek.signature(data, sk, pk) end
}

if cases["Ed25519"].() != cases["Dalek"].(), do: throw :mismatch

Benchee.run(cases)
