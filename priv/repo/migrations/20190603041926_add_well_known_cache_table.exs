defmodule CatChat.Repo.Migrations.AddWellKnownCacheTable do
  use Ecto.Migration

  def change do
    create table("well_known_cache", primary_key: false) do
      add :server_name, :string, primary_key: true
      add :m_server, :string
      add :last_backoff_secs, :integer
      add :valid_until, :utc_datetime, null: false
    end
  end
end
