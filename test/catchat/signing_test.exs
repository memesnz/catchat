defmodule CatChat.SigningTest do
  use ExUnit.Case, async: true

  def test_keypair do
    {sk, pk} =
      Ed25519.Dalek.generate_key_pair(
        CatChat.UnpaddedBase64.decode!("YJDBA9Xnr2sVqXD9Vj7XVUnmFZcZrlw8Md7kMW+3XA1")
      )

    %CatChat.KeyPair{
      sk: sk,
      pk: pk,
      algorithm: "ed25519",
      version: "1"
    }
  end

  def test_server_name do
    "domain"
  end

  def assert_signing_matches(orig, canon) do
    assert CatChat.JsonSigning.sign(Jason.decode!(orig), test_keypair(), test_server_name()) ==
             Jason.decode!(canon)
  end

  describe "json signer" do
    test "correctly signs first object from spec" do
      assert_signing_matches(
        """
        {}
        """,
        """
        {
          "signatures": {
            "domain": {
              "ed25519:1": "K8280/U9SSy9IVtjBuVeLr+HpOB4BQFWbg+UZaADMtTdGYI7Geitb76LTrr5QV/7Xg4ahLwYGYZzuHGZKM5ZAQ"
            }
          }
        }
        """
      )
    end

    test "correctly signs second object from spec" do
      assert_signing_matches(
        """
        {
          "one": 1,
          "two": "Two"
        }
        """,
        """
        {
          "one": 1,
          "signatures": {
            "domain": {
              "ed25519:1": "KqmLSbO39/Bzb0QIYE82zqLwsA+PDzYIpIRA2sRQ4sL53+sN6/fpNSoqE7BP7vBZhG6kYdD13EIMJpvhJI+6Bw"
            }
          },
          "two": "Two"
        }
        """
      )
    end
  end

  def assert_event_signing_matches(orig, canon) do
    assert CatChat.EventSigning.hash_and_sign(
             Jason.decode!(orig),
             test_keypair(),
             test_server_name()
           ) ==
             Jason.decode!(canon)
  end

  describe "event signer" do
    test "correctly signs first event from spec" do
      assert_event_signing_matches(
        """
        {
          "event_id": "$0:domain",
          "origin": "domain",
          "origin_server_ts": 1000000,
          "signatures": {},
          "type": "X",
          "unsigned": {
            "age_ts": 1000000
            }
        }
        """,
        """
        {
          "event_id": "$0:domain",
          "hashes": {
            "sha256": "6tJjLpXtggfke8UxFhAKg82QVkJzvKOVOOSjUDK4ZSI"
          },
          "origin": "domain",
          "origin_server_ts": 1000000,
          "signatures": {
            "domain": {
              "ed25519:1": "2Wptgo4CwmLo/Y8B8qinxApKaCkBG2fjTWB7AbP5Uy+aIbygsSdLOFzvdDjww8zUVKCmI02eP9xtyJxc/cLiBA"
            }
          },
          "type": "X",
          "unsigned": {
            "age_ts": 1000000
          }
        }
        """
      )
    end

    test "correctly signs second event from spec" do
      assert_event_signing_matches(
        """
        {
          "content": {
            "body": "Here is the message content"
          },
          "event_id": "$0:domain",
          "origin": "domain",
          "origin_server_ts": 1000000,
          "type": "m.room.message",
          "room_id": "!r:domain",
          "sender": "@u:domain",
          "signatures": {},
          "unsigned": {
          "age_ts": 1000000
          }
        }
        """,
        """
        {
          "content": {
            "body": "Here is the message content"
          },
          "event_id": "$0:domain",
          "hashes": {
            "sha256": "onLKD1bGljeBWQhWZ1kaP9SorVmRQNdN5aM2JYU2n/g"
          },
          "origin": "domain",
          "origin_server_ts": 1000000,
          "type": "m.room.message",
          "room_id": "!r:domain",
          "sender": "@u:domain",
          "signatures": {
            "domain": {
              "ed25519:1": "Wm+VzmOUOz08Ds+0NTWb1d4CZrVsJSikkeRxh6aCcUwu6pNC78FunoD7KNWzqFn241eYHYMGCA5McEiVPdhzBA"
            }
          },
          "unsigned": {
            "age_ts": 1000000
          }
        }
        """
      )
    end
  end
end
