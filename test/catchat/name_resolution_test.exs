defmodule CatChat.NameResolutionTest do
  use ExUnit.Case, async: true

  test "it correctly resolves an ipv4 address" do
    assert CatChat.NameResolution.resolve!("192.168.5.255") ==
             {"192.168.5.255", 8448, "192.168.5.255", nil}
  end

  test "it correctly resolves an ipv4 address with port" do
    assert CatChat.NameResolution.resolve!("192.168.5.255:347") ==
             {"192.168.5.255", 347, "192.168.5.255:347", nil}
  end

  test "it correctly resolves an ipv6 address" do
    assert CatChat.NameResolution.resolve!("[1234:5678::abcd]") ==
             {"[1234:5678::abcd]", 8448, "[1234:5678::abcd]", nil}
  end

  test "it correctly resolves an ipv6 address with port" do
    assert CatChat.NameResolution.resolve!("[1234:5678::abcd]:32") ==
             {"[1234:5678::abcd]", 32, "[1234:5678::abcd]:32", nil}
  end

  test "it correctly resolves a domain name with port" do
    assert CatChat.NameResolution.resolve!("example.com:443") ==
             {"example.com", 443, "example.com:443", "example.com"}
  end
end
