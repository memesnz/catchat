defmodule CatChat.NameResolution.WellKnownTest do
  use ExUnit.Case
  alias CatChat.NameResolution.WellKnown

  describe "cache-control parser" do
    test "parses correct cache-control headers" do
      assert WellKnown.max_age([{"cache-control", "public,  max-age=3600"}]) == 3600
      assert WellKnown.max_age([{"CaChE-CoNtRoL", "max-age=0,public"}]) == 0
    end

    test "returns nil without cache-control headers" do
      assert WellKnown.max_age([]) == nil
    end

    test "returns nil without max-age in the cache-control" do
      assert WellKnown.max_age([{"Cache-control", "immutable, awoo"}]) == nil
    end

    test "returns nil with invalid max-age in the cache-control" do
      assert WellKnown.max_age([{"Cache-control", "max-age=seventy-three"}]) == nil
    end
  end
end
