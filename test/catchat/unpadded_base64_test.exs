defmodule CatChat.UnpaddedBase64Test do
  use ExUnit.Case, async: true

  def assert_base64_encode_matches(orig, canon) do
    assert CatChat.UnpaddedBase64.encode(orig) ==
             canon
  end

  def assert_base64_decode_matches(orig, canon) do
    assert CatChat.UnpaddedBase64.decode!(orig) ==
             canon
  end

  test "it correctly encodes first case from spec" do
    assert_base64_encode_matches(
      "",
      ""
    )
  end

  test "it correctly encodes second case from spec" do
    assert_base64_encode_matches(
      "f",
      "Zg"
    )
  end

  test "it correctly encodes third case from spec" do
    assert_base64_encode_matches(
      "fo",
      "Zm8"
    )
  end

  test "it correctly encodes fourth case from spec" do
    assert_base64_encode_matches(
      "foo",
      "Zm9v"
    )
  end

  test "it correctly encodes fifth case from spec" do
    assert_base64_encode_matches(
      "foob",
      "Zm9vYg"
    )
  end

  test "it correctly encodes sixth case from spec" do
    assert_base64_encode_matches(
      "fooba",
      "Zm9vYmE"
    )
  end

  test "it correctly encodes seventh case from spec" do
    assert_base64_encode_matches(
      "foobar",
      "Zm9vYmFy"
    )
  end

  test "it correctly decodes first case from spec" do
    assert_base64_decode_matches(
      "",
      ""
    )
  end

  test "it correctly decodes second case from spec" do
    assert_base64_decode_matches(
      "Zg",
      "f"
    )
  end

  test "it correctly decodes third case from spec" do
    assert_base64_decode_matches(
      "Zm8",
      "fo"
    )
  end

  test "it correctly decodes fourth case from spec" do
    assert_base64_decode_matches(
      "Zm9v",
      "foo"
    )
  end

  test "it correctly decodes fifth case from spec" do
    assert_base64_decode_matches(
      "Zm9vYg",
      "foob"
    )
  end

  test "it correctly decodes sixth case from spec" do
    assert_base64_decode_matches(
      "Zm9vYmE",
      "fooba"
    )
  end

  test "it correctly decodes seventh case from spec" do
    assert_base64_decode_matches(
      "Zm9vYmFy",
      "foobar"
    )
  end
end
