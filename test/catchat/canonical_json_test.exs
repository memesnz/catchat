defmodule CatChat.CanonicalJsonTest do
  use ExUnit.Case, async: true

  def assert_canonicalisation_matches(orig, canon) do
    assert CatChat.CanonicalJson.encode!(Jason.decode!(String.trim_trailing(orig))) ==
             String.trim_trailing(canon)
  end

  test "it correctly encodes first case from spec" do
    assert_canonicalisation_matches(
      """
      {}
      """,
      """
      {}
      """
    )
  end

  test "it correctly encodes second case from spec" do
    assert_canonicalisation_matches(
      """
      {
          "one": 1,
          "two": "Two"
      }
      """,
      """
      {"one":1,"two":"Two"}
      """
    )
  end

  test "it correctly encodes third case from spec" do
    assert_canonicalisation_matches(
      """
      {
          "b": "2",
          "a": "1"
      }
      """,
      """
      {"a":"1","b":"2"}
      """
    )
  end

  test "it correctly encodes fourth case from spec" do
    assert_canonicalisation_matches(
      """
      {"b":"2","a":"1"}
      """,
      """
      {"a":"1","b":"2"}
      """
    )
  end

  test "it correctly encodes fifth case from spec" do
    assert_canonicalisation_matches(
      """
      {
          "auth": {
              "success": true,
              "mxid": "@john.doe:example.com",
              "profile": {
                  "display_name": "John Doe",
                  "three_pids": [
                      {
                          "medium": "email",
                          "address": "john.doe@example.org"
                      },
                      {
                          "medium": "msisdn",
                          "address": "123456789"
                      }
                  ]
              }
          }
      }
      """,
      """
      {"auth":{"mxid":"@john.doe:example.com","profile":{"display_name":"John Doe","three_pids":[{"address":"john.doe@example.org","medium":"email"},{"address":"123456789","medium":"msisdn"}]},"success":true}}
      """
    )
  end

  test "it correctly encodes sixth case from spec" do
    assert_canonicalisation_matches(
      """
      {
          "a": "日本語"
      }
      """,
      """
      {"a":"日本語"}
      """
    )
  end

  test "it correctly encodes seventh case from spec" do
    assert_canonicalisation_matches(
      """
      {
          "本": 2,
          "日": 1
      }
      """,
      """
      {"日":1,"本":2}
      """
    )
  end

  test "it correctly encodes eighth case from spec" do
    assert_canonicalisation_matches(
      """
      {
          "a": "\u65E5"
      }
      """,
      """
      {"a":"日"}
      """
    )
  end

  test "it correctly encodes ninth case from spec" do
    assert_canonicalisation_matches(
      """
      {
          "a": null
      }
      """,
      """
      {"a":null}
      """
    )
  end
end
