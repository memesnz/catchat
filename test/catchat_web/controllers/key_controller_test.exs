defmodule CatChatWeb.KeyControllerTest do
  use CatChatWeb.ConnCase

  describe "querying server directly" do
    test "works with and without key id", %{conn: conn} do
      r1 = conn |> get("/_matrix/key/v2/server") |> json_response(200)
      r2 = conn |> get("/_matrix/key/v2/server/ed25519:2") |> json_response(200)

      assert is_map(r1)

      assert r1 |> Map.drop(["signatures", "valid_until_ts"]) ==
               r2 |> Map.drop(["signatures", "valid_until_ts"])
    end
  end
end
