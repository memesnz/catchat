#[macro_use]
extern crate rustler;
extern crate ed25519_dalek;
extern crate lazy_static;
extern crate rustler_codegen;

use ed25519_dalek::*;
use rustler::{Binary, Encoder, Env, Error as NifError, NifResult, OwnedBinary, Term};

mod atoms {
    rustler_atoms! {
        atom ok;
        atom error;
        //atom __true__ = "true";
        //atom __false__ = "false";
    }
}

rustler_export_nifs! {
    "Elixir.Ed25519.Dalek",
    [("signature", 3, signature),
     ("generate_key_pair", 1, generate_key_pair),],
    None
}

fn raise_signature_error(e: SignatureError) -> NifError {
    NifError::RaiseTerm(Box::new((atoms::error(), e.to_string())))
}

fn signature<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    let data: Binary = args[0].decode()?;
    let sk: Binary = args[1].decode()?;
    let pk: Binary = args[2].decode()?;

    // copy_from_slice will panic and crash the VM if the key sizes are wrong... we don't want that.
    if sk.as_slice().len() != 32 || pk.as_slice().len() != 32 {
        return Err(NifError::RaiseAtom("bad_key"));
    }

    let mut key_material = [0; 64];
    key_material[0..32].copy_from_slice(sk.as_slice());
    key_material[32..64].copy_from_slice(pk.as_slice());

    // TODO figure out the security implications of this warning:

    // "Absolutely no validation is done on the key. If you give this function
    // bytes which do not represent a valid point, or which do not represent
    // corresponding parts of the key, then your Keypair will be broken and it
    // will be your fault."
    let kp = Keypair::from_bytes(&key_material).map_err(raise_signature_error)?;

    let sig = kp.sign(data.as_slice());

    let mut res = OwnedBinary::new(64).ok_or(NifError::RaiseAtom("cant_alloc_binary"))?;
    res.as_mut_slice().copy_from_slice(&sig.to_bytes());

    Ok(res.release(env).encode(env))
}

fn generate_key_pair<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    let sk_bytes: Binary = args[0].decode()?;

    if sk_bytes.as_slice().len() != 32 {
        return Err(NifError::RaiseAtom("bad_key"));
    }

    let sk = SecretKey::from_bytes(sk_bytes.as_slice()).map_err(raise_signature_error)?;
    let pk: PublicKey = (&sk).into();

    let mut pk_bytes = OwnedBinary::new(32).ok_or(NifError::RaiseAtom("cant_alloc_binary"))?;
    pk_bytes.as_mut_slice().copy_from_slice(&pk.to_bytes());

    Ok((sk_bytes, pk_bytes.release(env)).encode(env))
}
